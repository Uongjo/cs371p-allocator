// --------------
// RunVoting.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include "Allocator.h"

// ----
// main
// ----

int main () {
    using namespace std;
    allocatorSolve(cin, cout);
    return 0;
}
