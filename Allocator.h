// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

using namespace std;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    /**
     * Equality operator
     */
    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }

    // -----------
    // operator !=
    // -----------

    /**
     * Inequality operator
     */
    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];
    const int TYPE_SIZE;
    const int HEAP_SIZE;
    int* start;
    int* finish;

    // -----
    // valid
    // -----

    /**
     * const helper method for valid
     * @param p an int pointer to header sentinel
     * @return pointer to footer sentinel from header sentinel
     */
    const int* getFooter(int* p) const {
        int bytes = abs(*p);
        p += 1 + (bytes/4);
        return p;
    }

    /**
     * O(1) in space
     * O(n) in time
     * valid checks for: size of all blocks add to 1000
     *                   sentinels match in magnitude and sign
     *                   makes sure sentinel != 0
     *                   makes sure no two free blocks in a row
     * @return whether true if the heap is valid, false otherwise
     */
    bool valid () const {
        iterator b = begin();
        iterator e = end();
        bool previousFree = false;
        int* p;
        int size = 0;
        while(b != e) {
            size += 8;                    // size of sentinels
            p = (int*)&(*b);
            if(*p != *getFooter(p))       // Checks if sentinels are equal
                return false;
            if(*p == 0)                   // Checks if sentinels are 0
                return false;
            if(*p > 0) {
                if(previousFree)          // Checks for two free blocks in a row
                    return false;
                else
                    previousFree = true;
            } else {
                previousFree = false;
            }
            size += abs(*p);              // Size of data
            ++b;
        }
        if(size != HEAP_SIZE)             // Checks if all numbers add up to total heap size
            return false;
        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------


        /**
         * Equality operator
         * @param lhs an my_allocator::iterator
         * @param rhs an my_allocator::iterator
         * @return true if inner pointers are equal, false otherwise
         */
        friend bool operator == (const iterator& rhs, const iterator& lhs) {
            return rhs._p == lhs._p;
        }

        // -----------
        // operator !=
        // -----------

        /**
         * Inequality operator
         * @param lhs an my_allocator::iterator
         * @param rhs an my_allocator::iterator
         * @return true if inner pointers are not equal, false otherwise
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:

        // -----------
        // constructor
        // -----------

        /**
         * Iterator constructor
         * @param p an int pointer
         */
        iterator (int* p) : _p(p) {}

        // ----------
        // operator *
        // ----------

        /**
         * * operator
         * @return const reference to int that _p is pointing to
         */
        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * Pre-inc operator
         * @return l-value iterator moving to next sentinel
         */
        iterator& operator ++ () {
            int size = abs(*_p);
            _p += (size/4) + 2; // 4 --> sizeof(int)
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * Post-inc operator
         * @return r-value iterator moving to next sentinel
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;      // Use pre_inc
            return (iterator)x;   // cpp check yelled at me for reference to auto so
        }
    };

    /**
     * Begin for iterator
     * @return my_allocator::iterator at beginning of heap
     */
    iterator begin () const {
        return (int*)a;
    }

    /**
     * End for iterator
     * @return my_allocator::iterator at end of heap
     */
    iterator end () const {
        return (int*)(a + HEAP_SIZE);
    }

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     * Constructor for my_allocator
     */
    my_allocator () : TYPE_SIZE(sizeof(T)), HEAP_SIZE((int)N), start((int*)a), finish( (int*)(a + HEAP_SIZE - 4) ) {
        if(HEAP_SIZE < TYPE_SIZE + 8)
            throw bad_alloc();
        // Initializing beginning and end sentinels
        *(int*)a = HEAP_SIZE - 8;
        *(int*)(a + HEAP_SIZE - 4) = HEAP_SIZE - 8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * helper method to get sentinel blocks
     * @param p an int pointer to header sentinel
     * @return int pointer to footer sentinel from header sentinel
     */
    int* getFooter(int* p) {
        int bytes = abs(*p);
        p += 1 + (bytes/4);
        return p;
    }

    /**
     * helper method to get sentinel blocks
     * @param p an int pointer to footer sentinel
     * @return int pointer to header sentinel from footer sentinel
     */
    int* getHeader(int* p) {
        int bytes = abs(*p);
        p -= 1 + (bytes/4);
        return p;
    }

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * @param n number of objects of sizeof(T)
     * @return pointer of type T to start of data block
     */
    pointer allocate (size_type n) {
        if(n == 0) {
            throw bad_alloc();
        }
        int bytes = n * TYPE_SIZE;
        iterator b = begin();
        iterator e = end();
        while(*b < bytes) {
            if(b == e) {                // If we reach the end of the heap, throw exception
                throw bad_alloc();
            }
            ++b;
        }
        int* p = (int*)&(*b);
        pointer returnPtr = (pointer)(p + 1);
        int remainder = *p - bytes - 8; // Subtract 8 to account for new header/footer sentinels
        if(remainder > 0) {
            *p = -bytes;
            p = getFooter(p);
            *p = -bytes;
            p += 1;                    // spliting block
            *p = remainder;
            *getFooter(p) = remainder;
        } else {
            int size = *p;
            *p = -size;
            p = getFooter(p);
            *p = -size;
        }
        assert(valid());
        return returnPtr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * Helper method to merge blocks
     * Merges if left block is free
     * @param p an int pointer to current free block
     * @return int pointer to new block if merging left sucessful, p otherwise
     */
    int* mergeLeft(int* p) {
        int* leftBlock = getHeader(p - 1);      // get sentinel of left block
        if(*leftBlock > 0) {                    // If free, merge
            int newSize = *leftBlock + *p + 8;
            *leftBlock = newSize;
            *getFooter(leftBlock) = newSize;
            p = leftBlock;
        }
        return p;
    }

    /**
     * Helper method to merge blocks
     * Merges if right block is free
     * @param p an int pointer to current free block
     * @returns pointer to current block
     */
    int* mergeRight(int* p) {
        int* rightBlock = getFooter(p) + 1;     // get sentinel of right block
        if(*rightBlock > 0) {                   // If free, merge
            int newSize = *rightBlock + *p + 8;
            *p = newSize;
            *getFooter(p) = newSize;
        }
        return p;
    }

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * Frees block given pointer to data and merges adjacent free blocks
     * @param _p a pointer to the block of data to be freed
     */
    void deallocate (pointer _p, size_type) {
        int* p = ((int*)_p) - 1;        // get sentinel
        if(p > finish || p < start)     // check if argument is vald
            throw invalid_argument("Invalid pointer");
        *p = abs(*p);
        *getFooter(p) = *p;
        if((getFooter(p) + 1) < finish) // Make sure we're in bounds of heap
            mergeRight(p);
        if(p - 1 > start)               // Make sure we're in bounds of heap
            mergeLeft(p);
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }
};

void allocatorSolve(istream& r, ostream& w);

#endif // Allocator_h
