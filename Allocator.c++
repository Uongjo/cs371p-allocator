#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include "Allocator.h"

using namespace std;


void allocatorSolve(istream& r, ostream& w) {
    int cases;
    r >> cases;
    assert(cases <= 100);
    assert(cases > 0);
    {   // get rid of empty input line
        string garbage;
        getline(r, garbage);
        getline(r, garbage);
    }
    while (cases-- > 0) {
        my_allocator<double, 1000> heap;
        string line;
        getline(r, line);
        int numLines = 0;
        while (line != "") {
            int nextNum = stoi(line);
            if(nextNum > 0) {           // If positive, allocate
                heap.allocate(nextNum);
            } else if(nextNum < 0) {    // If negative, deallocate
                int busyBlock = abs(nextNum) - 1;
                auto b = heap.begin();
                while(*b > 0) { // get the first busyBlock
                    ++b;
                }
                while(busyBlock > 0) { // get the right busy block
                    do {
                        ++b;
                    } while(*b > 0);
                    --busyBlock;
                }
                int* p = (int*)&(*b);
                heap.deallocate((double*)(p + 1), 0); // give address of data that needs to be freed
            }
            getline(r, line);
            ++numLines;
        }
        assert(numLines <= 1000);
        my_allocator<double, 1000>::iterator b = begin(heap);
        my_allocator<double, 1000>::iterator e = end(heap);
        while(b != e) { // Iterate through sentinels and print
            w << *b;
            ++b;
            if(b != e)
                w << " ";
        }
        w << "\n";
    }
}
